package com.ustb.tree;

import com.ustb.dao.PolicyDao;
import com.ustb.tree.entity.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {

    /**
     * 测试
     * @param args
     */
    public static void main(String[] args){
//        Target target1 =new Target("policy1");
//        target1.addAttr("Role#Sub","doctor");
//        target1.addAttr("Role#Sub","intern");
//        target1.addAttr("Resource-id#Res","script");
//        target1.addAttr("Action-id#Act","read");
//
//        Target target2 =new Target("policy2");
//        target2.addAttr("Role#Sub","intern");
//        target2.addAttr("Resource-id#Res","script");
//        target2.addAttr("Action-id#Act","read");
//
//        Target target3 =new Target("policy3");
//        target3.addAttr("Role#Sub","doctor");
//        target3.addAttr("Resource-id#Res","script");
//        target3.addAttr("Action-id#Act","create");
//
//        Target target4 =new Target("policy4");
//        target4.addAttr("Sex#Sub","women");
//        target4.addAttr("Role#Sub","doctor");
//        target4.addAttr("Resource-id#Res","script");
//        target4.addAttr("Action-id#Act","create");
//
//        Target target5 =new Target("policy5");
//        target5.addAttr("Role#Sub","doctor");
//        target5.addAttr("Resource-id#Res","script");
//        target5.addAttr("Action-id#Act","write");
//
//       // List<Target> formatedTarget=Target.formateTarget(target);
//        for(int i=0;i<formatedTarget.size();i++){
//            formatedTarget.get(i).printTarget();
//        }
//        ID3 id3=new ID3();
//        TreeNode root=id3.buildTree(new Target[]{target1,target2,target3,target4,target5});
//
//
//        AccessRequest request=new AccessRequest();
//        request.attr.put("Role#Sub","doctor");
//        request.attr.put("Action-id#Act","create");
//        request.attr.put("Resource-id#Res","script");
//
//
//        long start=System.currentTimeMillis();
//        List<LeafNode> result=id3.match(root,request);
//        System.out.println("匹配耗时"+(System.currentTimeMillis()-start));
//        System.out.println();

        for(int i=1;i<=200;i++){
            List<FomateTarget> targets= PolicyDao.getTargets(0,i);
            ID3 id3=new ID3();
            for(int j=0;j<10;j++){
                Long start=System.nanoTime()/1000L;
                TreeNode root=id3.buildTree(targets);
                System.out.print(System.nanoTime()/1000L-start+"\t");
            }
            System.out.println();

        }

        System.out.println();
    }
}
