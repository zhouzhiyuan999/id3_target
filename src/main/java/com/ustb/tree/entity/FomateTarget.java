package com.ustb.tree.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * 格式化的策略目标，每个属性只有一个属性值
 */
public class FomateTarget {
    private Map<String,Object> attributes=new HashMap<>();
    private String policyId;

    public FomateTarget() {
    }

    public FomateTarget(String policyId) {
        this.policyId = policyId;
    }

    public FomateTarget(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public void put(String name,Object value){
        this.attributes.put(name,value);
    }
    public Object getValue(String name){
        return this.attributes.get(name);
    }

    public Boolean containsKey(String name){
        return this.attributes.containsKey(name);
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }
}
