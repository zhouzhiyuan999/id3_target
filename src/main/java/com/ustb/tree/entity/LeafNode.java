package com.ustb.tree.entity;

import java.util.ArrayList;
import java.util.List;

public class LeafNode extends TreeNode {
    private List<String> policyIds;


    public LeafNode() {
        this.policyIds = new ArrayList<>();
    }

    public List<String> getPolicyIds() {
        return policyIds;
    }

    public void setPolicyIds(List<String> policyIds) {
        this.policyIds = policyIds;
    }

    public void addPolicyId(String policyId){
        this.policyIds.add(policyId);
    }
}
