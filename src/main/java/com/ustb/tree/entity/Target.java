package com.ustb.tree.entity;

import com.ustb.tree.ID3;

import java.lang.reflect.Array;
import java.util.*;

public class Target {

    private Map<String,Attribute> attr;
    private String policyId;

    public Target(String policyId) {
        this.attr=new HashMap<>();
        Attribute<String> attr=new Attribute("policyId",policyId);
        this.attr.put("policyId",attr);
        this.policyId=policyId;
    }

    public Map<String, Attribute> getAttr() {
        return attr;
    }
    public Attribute getAttr(String name) {
        return attr.get(name);
    }

    public void setAttr(Map<String, Attribute> attr) {
        this.attr = attr;
    }

    public void addAttr(Attribute attribute){
        if(attr.containsKey(attribute.getName())){
            attr.get(attribute.getName()).addValue(attribute.getValues());
        }else{
            attr.put(attribute.getName(),attribute);
        }
    }

    public void addAttr(String name,Object value){
        if(attr.containsKey(name)){
            attr.get(name).addValue(value);
        }else{
            Attribute attr=new Attribute(name,value);
            this.attr.put(name,attr);
        }
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public Boolean containsKey(String name){
        return this.attr.containsKey(name);
    }

    /**
     * 将策略目标格式化成析取范式
     * @param target
     * @param attributeSet 全属性集合
     * @return
     */
    public static List<FomateTarget> formateTarget(Target target,final Set<String> attributeSet){
        //先计算总数
        int total=1;
        for(String key:attributeSet){
            total*=target.attr.containsKey(key)?target.attr.get(key).getValueCount():1;
        }
        FomateTarget[] arrays=new FomateTarget[total];

        for(String key:attributeSet){
            int count=target.attr.containsKey(key)?target.attr.get(key).getValueCount():1;
            for(int i=0;i<total;i+=count){
                int c=0;
                Set values;
                if(target.attr.containsKey(key)){
                    values=target.attr.get(key).getValues();
                }else{
                    values=new HashSet<>();
                    values.add(ID3.NULL_VALUE);
                }
                for(Object value:values){
                    if(arrays[i+c]==null){
                        arrays[i+c]=new FomateTarget(target.policyId);
                    }
                    arrays[i+c].put(key,value);
                    c++;
                }

            }
        }
        return Arrays.asList(arrays);
    }


    public void printTarget(){
        Set<String> keys=attr.keySet();
        for(String key:keys){
            Set values=attr.get(key).getValues();
            for(Object value:values){
                System.out.println("attr:"+key+",\t value:"+value);
            }
        }
        System.out.println();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Target target = (Target) o;
        if(!Objects.equals(policyId, target.policyId)){
            return false;
        }
        for(String key:attr.keySet()){
            if(target.attr.containsKey(key)){
                if(!target.attr.get(key).equals(attr.get(key))){
                    return false;
                }
            }else {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {

        return Objects.hash(attr, policyId);
    }
}
