package com.ustb.tree.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TreeNode<T> {

    private String name;//当前节点的测试属性
    private Map<T,TreeNode<?>> children;    //非叶子节点

    public TreeNode() {
    }

    public TreeNode(String name) {
        this.name = name;
        this.children=new HashMap<>();

    }

    public TreeNode(String name, Map<T, TreeNode<?>> children) {
        this.name = name;
        this.children = children;
    }

    public void addChild(T value,TreeNode<?> child){
        this.children.put(value,child);
    }

    public Map<T, TreeNode<?>> getChildren() {
        return children;
    }

    public String getName() {
        return name;
    }

    public TreeNode getChild(T value){
        return this.children.get(value);
    }
}
