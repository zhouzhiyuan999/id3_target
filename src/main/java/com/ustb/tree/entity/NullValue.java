package com.ustb.tree.entity;

import java.util.Objects;

/**
 * 用于表示null值
 */
public class NullValue {

    private int value=-1;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NullValue nullValue = (NullValue) o;
        return value == nullValue.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
