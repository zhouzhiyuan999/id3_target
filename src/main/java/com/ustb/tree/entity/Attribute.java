package com.ustb.tree.entity;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Attribute<T> implements Cloneable {
    private String name;
    private Set<T> values;

    public Attribute(String name, Set<T> values) {
        this.name = name;
        this.values = values;
    }

    public Attribute(String name,T value) {
        this.name = name;
        this.values=new HashSet<>();
        this.values.add(value);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<T> getValues() {
        return values;
    }

    public Integer getValueCount(){
        return values.size();
    }
    public void setValues(Set<T> values) {
        this.values = values;
    }

    public void addValue(T value){
        this.values.add(value);
    }

    public boolean contains(T value){
        return this.values.contains(value);
    }

    public void addValue(Set<T> values){
        this.values.addAll(values);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Attribute<?> attribute = (Attribute<?>) o;
        if(values.size()!=attribute.values.size()){
            return false;
        }
        for(T value:values){
            if(!attribute.values.contains(value)){
                return false;
            }
        }
        return Objects.equals(name, attribute.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, values);
    }

    public Attribute<T> clone(){
        Set<T> newValue=new HashSet<>();
        for(T value:this.values){
            newValue.add(value);
        }
        Attribute result=new Attribute(this.name,newValue);
        return result;
    }
}
