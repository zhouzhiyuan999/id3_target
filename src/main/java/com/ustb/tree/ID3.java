package com.ustb.tree;

import com.ustb.tree.entity.*;

import java.lang.reflect.Array;
import java.util.*;

public class ID3 {

    private Map<String,Attribute> attributes=new HashMap<>();

    public  static NullValue NULL_VALUE=new NullValue();
    static String TARGET_ATTR="policyid";
    public ID3() {
    }


    public List<LeafNode> match(TreeNode root,AccessRequest request){
        List<LeafNode> result=new ArrayList<>();
        if(root!=null){
            if(root instanceof LeafNode){
                result.add((LeafNode)root);
                return result;
            }else{
                String name=root.getName();
                if(request.attr.containsKey(name)){
                    Object value=request.attr.get(name);
                    if(root.getChildren().containsKey(value)){
                        result.addAll(match(root.getChild(value),request));
                    }
                }
                if(root.getChildren().containsKey(NULL_VALUE)){
                    result.addAll(match(root.getChild(NULL_VALUE),request));
                }
            }
        }
        return result;
    }

    public TreeNode buildTree(Target[] targets){
        //抽取属性集合
        //可用属性列表
        Set<String> candidate=new HashSet<>();
        for(int i=0;i<targets.length;i++){
            for(String key:targets[i].getAttr().keySet()){
                if(attributes.containsKey(key)){
                    attributes.get(key).addValue(targets[i].getAttr().get(key).getValues());
                }else{
                    candidate.add(key);
                    attributes.put(key,targets[i].getAttr().get(key).clone());
                }
            }
        }
        //格式化target
        List<FomateTarget> formatedTargets=formatTargets(targets,candidate);
        candidate.remove(TARGET_ATTR);
        //构建树
        return buildTree(formatedTargets,candidate);
    }

    public TreeNode buildTree(List<FomateTarget> targets){
        Set<String> candidate=new HashSet<>();
        //构建属性集合
        for(int i=0;i<targets.size();i++){
            for(String key:targets.get(i).getAttributes().keySet()){
                if(attributes.containsKey(key)){
                    attributes.get(key).addValue(targets.get(i).getValue(key));
                }else{
                    candidate.add(key);
                    attributes.put(key,new Attribute(key,targets.get(i).getValue(key)));
                }
            }
        }
       // List<FomateTarget> formatedTargets=formatTargets(targets,candidate);
        candidate.remove(TARGET_ATTR);
        //构建树
        return buildTree(targets,candidate);
    }


    private TreeNode buildTree(List<FomateTarget> targets,Set<String> candidate){
        TreeNode root;
        if(candidate.size()==0){
            root= new LeafNode();
            for(FomateTarget t:targets){
                ((LeafNode)root).addPolicyId(t.getPolicyId());
            }
        }else {
            //选择最佳分类属性
            Attribute best=selectBestAttribute(targets,candidate);
            root=new TreeNode(best.getName());
            Map<Object,List<FomateTarget>> prob=this.getProbDistribution(targets,best.getName());
            candidate.remove(best.getName());
            for(Object value:prob.keySet()){
                root.addChild(value,buildTree(prob.get(value),candidate));
            }
            //在递归时，每一集算完都要添加回删除的属性
            candidate.add(best.getName());
        }
        return root;

    }


    private Attribute selectBestAttribute(List<FomateTarget> targets,Set<String> candidate){
        double maxGain=0;
        String result=null;
        for(String key : candidate){
            double gain=this.Gain(targets,TARGET_ATTR,key);
            if(gain>maxGain){
                result=key;
                maxGain=gain;
            }
            if(gain==0&& result==null && targets.get(0).containsKey(key)!=null){
                //gain都为0时,即只剩下一个样例时任选一个样例中存在的属性作为分类属性
                //gain
                result=key;
            }
        }
        return this.attributes.get(result);
    }

    private double Gain(List<FomateTarget> targets,String targetAttr,String attribute){
        double result=this.Entropy(targets,targetAttr);
        Map<Object,List<FomateTarget>> prob=this.getProbDistribution(targets,attribute);
        int length=targets.size();
        for(Object key: prob.keySet()){
            double pi=(double)prob.get(key).size()/length;
            result-=pi*this.Entropy(prob.get(key),targetAttr);
        }
        return result;
    }

    /**
     * 计算样例在目标属性下的熵
     */
    private double Entropy(List<FomateTarget>examples,String targetAttr){
        double result=0.0;
        Map<Object,List<FomateTarget>> prob=this.getProbDistribution(examples,targetAttr);
        int length=examples.size();
        for(Object key : prob.keySet()){
            double pi=(double)prob.get(key).size()/length;
            result-=pi*Math.log(pi);
        }
        return result;
    }

    /**
     * 得到样例在目标属性下的概率分布
     * 返回
     */
    private Map<Object,List<FomateTarget>> getProbDistribution(List<FomateTarget> targets,String testAttr){
        Map<Object,List<FomateTarget>> prob=new HashMap<>();
        for(int i=0;i<targets.size();i++){
            Object value=targets.get(i).getValue(testAttr);
            if(prob.containsKey(value)){
                prob.get(value).add(targets.get(i));
            }else{
                List<FomateTarget> temp=new ArrayList<>();
                temp.add(targets.get(i));
                prob.put(value,temp);
            }
        }
        return prob;
    }





    /**
     * 格式化目标集合，最终得到的目标集合同时也包括null值
     * @param targets
     * @param attributeSet
     */
    private List<FomateTarget> formatTargets(Target[] targets, final Set<String> attributeSet){
        Set<FomateTarget> result=new HashSet<>();
        for(int i=0;i<targets.length;i++){
            result.addAll(Target.formateTarget(targets[i],attributeSet));
        }
        //清除相同的Target
        return new ArrayList<>(result);
    }
}
