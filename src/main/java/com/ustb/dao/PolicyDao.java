package com.ustb.dao;

import com.ustb.tree.ID3;
import com.ustb.tree.entity.FomateTarget;
import com.ustb.util.JdbcUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PolicyDao {

    public static List<FomateTarget> getTargets(Integer start,Integer end) {
        List<FomateTarget> result=new ArrayList<>();
        String sql = "select * from t_policy limit " + start + "," + end;
        Connection conn = JdbcUtil.getConn();
        Statement stmt = null;
        ResultSet ret = null;
        try {
            stmt = conn.createStatement();
            //执行语句，得到结果集
            ret = stmt.executeQuery(sql);
            ResultSetMetaData rsmd=ret.getMetaData();
            while (ret.next()) {
                //这里只查询的密码
                FomateTarget fomateTarget=new FomateTarget();
                fomateTarget.setPolicyId(ret.getString("policyid"));
                for(int i=1;i<=rsmd.getColumnCount();i++){
                    Object value=ret.getObject(i);
                    if(value==null|| value.equals("") ){
                        fomateTarget.put(rsmd.getColumnName(i), ID3.NULL_VALUE);
                    }else{
                        fomateTarget.put(rsmd.getColumnName(i),ret.getObject(i));
                    }
                }
                result.add(fomateTarget);
            }
            ret.close();
            conn.close();//关闭连接
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        return result;

    }

}
