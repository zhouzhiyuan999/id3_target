/**
 * Created by admin on 2018/11/8 0008.
 */


function buildTree(nodesdata){
    var dataMap = nodesdata.reduce(function(map, node) {
        map[node.name] = node;
        return map;
    }, {});

    // create the tree array
    var treeData = [];
    nodesdata.forEach(function(node) {
        // add to parent
        var parent = dataMap[node.parent];
        if (parent) {
            // create child array if it doesn't exist
            (parent.children || (parent.children = []))
            // add node to child array
                .push(node);
        } else {
            // parent is null or missing
            treeData.push(node);
        }
    });

    // ************** Generate the tree diagram	 *****************
    var margin = {top: 20, right: 120, bottom: 20, left: 120},
        width = 1200 - margin.right - margin.left,
        height = 1200 - margin.top - margin.bottom;

    var i = 0;

    var tree = d3.layout.tree().size([height, width]);

    var diagonal = d3.svg.diagonal()
        .projection(function(d) { return [d.y, d.x]; });

    var svg =d3.select("body").append("svg")
            .attr("width", width + margin.right + margin.left)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    root = treeData[0];

    update(root);

    function update(source) {

        // Compute the new tree layout.
        var nodes = tree.nodes(root).reverse(),
            links = tree.links(nodes);

        // Normalize for fixed-depth.
        nodes.forEach(function (d) {
            let interval=30;
            let brother=(d.parent==null?1:d.parent.children.length);
            let y=1;
            if(brother==1){
                y=120;
            }else{
                y=d.depth *brother*interval;
            }
            let pnode=d.parent;
            while(pnode){
                let parent_brother=(pnode.parent==null?1:pnode.parent.children.length);
                y+=(parent_brother==1?120:pnode.depth*parent_brother*interval);
                pnode=pnode.parent;
            }
            d.y = y;
        });

        // Declare the nodes…
        var node = svg.selectAll("g.node")
            .data(nodes, function (d) {
                return d.id || (d.id = ++i);
            });

        // Enter the nodes.
        var nodeEnter = node.enter().append("g")
            .attr("class", function(d){
                return d.children||d._children?"node":"leafnode";
            })
            .attr("transform", function (d) {
                return "translate(" + d.y + "," + d.x + ")";
            });

        nodeEnter.append("circle")
            .attr("r", 10)
            .style("fill",function (d) {
                return d.children||d._children?"#fff":"#ddd";
            } );

        nodeEnter.append("text")
            .attr("x", function (d) {
                // return d.children || d._children ? -13 : 13;
                return -20;
            })
            .attr("dy", "-15")
            .attr("text-anchor", function (d) {
                // return d.children || d._children ? "end" : "start";
                return "start"
            })
            .text(function (d) {
                return d.attr;
            })
            .style("fill-opacity", 1);

        // Declare the links…
        var linkEnter = svg.selectAll("path.link").append("g")
            .data(links, function (d) {
                return d.target.id;
            });

        linkEnter.enter().insert("path", "g")//在指定元素之前插入一个元素
            .attr("class", "link")
            .attr("d", diagonal)
            // 首先为每条节点连线添加标识id
            .attr("id", function (d, i) {
                return "mypath" + i;
            });

        //为连线添加文字
        linkEnter.enter().append('text')
            .attr('x', function(d){
                if(d.target.parent.children.length==1){
                    return 40;
                }else{
                    var xdist=Math.pow((d.target.x-d.source.x),2);
                    var ydist=Math.pow((d.target.y-d.source.y),2);
                    return Math.pow(xdist+ydist,0.5)/2;
                }
            })
            .attr('dy', function(d){
                if(d.target.parent.children.length==1){
                    return 5;
                }else{
                    return -3;
                }
            })
            .style('fill', 'green')
            .style('font-size', '10px')
            .style('font-weight', 'bold')
            .append('textPath')
            .attr({//引用路径
                'xlink:href': function (d, i) {
                    return "#mypath" + i;
                }
            })
            .text(function (d, i) {
                return d.target.value;
            });
    }
}