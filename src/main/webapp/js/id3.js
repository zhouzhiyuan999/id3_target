
function ID3(examples){
	if(!$.isArray(examples)){
		return null;
	}
	this.root={};
	this.attr={};
	this.originData=[];
	Object.assign(this.originData,examples);
	this.data=[];
	this.preHandler();
	this.root=this.buildTree(this.data,"policyId",this.attr);
}

/**
* 预处理样本数据
*  [
*	 {
*	 	policyId:"001",
*	 	target:[
*			{attribute:"Role#Sub",value:"doctor"},
*			{attribute:"Role#Sub",value:"intern"},
*			{attribute:"Resource-id#Res",value:"read"},
*			{attribute:"Action-id#Act",value:"read"},
*			{attribute:"Action-id#Act",value:"write"} 
*	 	]
*	 },
*	 ...
* ]
*转换后的格式
* [
* 		{"policyId":"001","Role#Sub":"doctor","Resource-id#Res":"script","Action-id#Act":"read"},
*			{"policyId":"001","Role#Sub":"doctor","Resource-id#Res":"script","Action-id#Act":"write"},
*	]
 * 
 */
ID3.prototype.preHandler=function(){
	for(let i=0;i<this.originData.length;i++){
		let policyId=this.originData[i].policyId;
		let target=[];
		Object.assign(target,this.originData[i].target);
		//将不同的属性和属性值提取出来放入属性集合中
		for(let j=0;j<target.length;j++){
			let name=target[j].attribute;
			let value=target[j].value;
			if(this.attr[name]==null){
				this.attr[name]=new Set();
			}
			this.attr[name].add(value);
		}
		target.push({"attribute":"policyId","value":policyId});

		//格式化target结构
		this.data.push.apply(this.data,this.formatTarget(target));
	}
}
 
 
 /**
  * 将target由合取式转变为析取式
  * 返回
* [
* 		{"policyId":"001","Role#Sub":"doctor","Resource-id#Res":"script","Action-id#Act":"read"},
*			{"policyId":"001","Role#Sub":"doctor","Resource-id#Res":"script","Action-id#Act":"write"},
* ...
*	]
  */
ID3.prototype.formatTarget=function(target){
	//将不同的属性和属性值提取出来放入属性集合中
	let attrlist={};
	for(let i=0;i<target.length;i++){
		let name=target[i].attribute;
		let value=target[i].value;
		if(attrlist[name]==null){
			attrlist[name]=[];
		}
		attrlist[name].push(value);
	}
	
	
	let size=1;
	for(let key in attrlist){
		size*=attrlist[key].length;
	}
	let result=new Array(size);	

	let keys=Object.keys(attrlist);
	for(let i=0;i<keys.length;i++){
		let key=keys[i];
		let valuelength=attrlist[keys[i]].length;
		//计算后续集合的笛卡尔积个数
		let descartes=1;
		for(let j=i+1;j<keys.length;j++){
			descartes*=attrlist[keys[j]].length;
		}
		//确认当前每个元素循环次数
		for(let j=0;j<size/(descartes*valuelength);j++){
			for(let k=0;k<valuelength;k++){		
				//确认每个元素循环打印次数
				for(let l=0;l<descartes;l++){
					let index=l+k*descartes+j*descartes*valuelength;
					if(result[index]==null){
						result[index]={};
					}
					result[index][key]=attrlist[key][k];
				}
			}
			
		}
	}
	return result;
	
}

/**
* 创建使用ID3算法的target查找树，json表示
* 单个样例结构
* {
* policyId:"001",
* target:[
*	[
*		{attribute:"Role#Sub",value:"doctor"},
*		{attribute:"Resource-id#Res",value:"read"},
*		{attribute:"Action-id#Act",value:"read"}
*	],
*	[
*		{attribute:"Role#Sub",value:"doctor"},
*		{attribute:"Resource-id#Res",value:"read"},
*		{attribute:"Action-id#Act",value:"write"}
*	],
*	...
* ]
* }
*返回子树的root节点
* {
*	 attribute:"Role#Sub",
*	 children:[
*		 {value:"doctor",node:{ctree1...}},
*		 {value:"intern",node:{ctree2...}},
*		 {value:"student",node:{}}
*		 ...
*	 ]
* }
* 
*/
ID3.prototype.buildTree=function(examples,targetAttr,attributes){
	let root={};
	//判断训练集是否为空
	if(examples==null|| examples.length==0){
		return null;
	}
	//判断样本是否都属于同一个policy
	let idSet=this.isSameType(examples,targetAttr);
	if(idSet.size==1){
		//样例中的所有属性都在路径上了
		// let temp=true;
		// for(let key in examples[0]){
		// 	if(attributes[key]!=null){
         //        temp=false;
		// 		break;
		// 	}
		// }
		// if(temp){
         //    return Array.from(idSet) ;
		// }
		return Array.from(idSet);
	}
	//只有属性列表为空才能返回
	if(attributes==null || attributes.length==0||Object.keys(attributes).length === 0){
		return Array.from(idSet);
	}
	
	//选择分类能力最好的属性
	let attr=this.selectBestAttr(examples,targetAttr,attributes);
    
    //剩余属性不在样例集合中
    if(attr==null){
        return Array.from(idSet);
    }
	root["name"]=attr;
	root["children"]={};
	let children=this.getProbDistribution(examples,attr,true);
	for(let key in children){
		//集合遍历
		let childAttributes={};
		Object.assign(childAttributes,attributes)
		//childAttributes.splice(childAttributes.index)
		delete childAttributes[attr];
		root["children"][key]=this.buildTree(children[key],targetAttr,childAttributes);
	}
	return root;
	
}

/**
 * 判断所有target是否相同，返回id数组
 */
ID3.prototype.isSameType=function(examples,targetAttr){
	let idSet=new Set();
	let targetValue=examples[0][targetAttr];
	idSet.add(targetValue)
	for(let i=1;i<examples.length;i++){
		if(examples[i][targetAttr]!=targetValue){
			idSet.add(examples[i][targetAttr]);
		}
	}
	return idSet;
}

/**
 * 得到分类能力最好的属性
 */
ID3.prototype.selectBestAttr=function(examples,targetAttr,attributes){
		let maxGain=0;
		let result=null;
		for(let key in attributes){
			let gain=this.Gain(examples,targetAttr,key);
			if(gain>maxGain){
				result=key;
				maxGain=gain;
			}
			if(gain==0&& result==null && examples[0][key]!=null){
                //gain都为0时,即只剩下一个样例时任选一个样例中存在的属性作为分类属性
				result=key;
			}
		}
		return result;
}

/**
 * 
 *计算examples在待测试属性下的信息增益
 * targetAttr:目标属性
 * testAttr:待测试的属性
 */
ID3.prototype.Gain=function(examples,targetAttr,testAttr){
	let result=this.Entropy(examples,targetAttr);
	let prob=this.getProbDistribution(examples,testAttr,true);
	let length=examples.length;
	for(let key in prob){
		let pi=prob[key].length/length;
		result-=pi*this.Entropy(prob[key],targetAttr);
	}
	return result;
}

/**
 * 计算样例在目标属性下的熵
 */
ID3.prototype.Entropy=function(examples,targetAttr){
	let result=0;
	let prob=this.getProbDistribution(examples,targetAttr,true);
	let length=examples.length;
	for(let key in prob){
		let pi=prob[key].length/length;
		result-=pi*Math.log2(pi);
	}
	return result;
}


/**
 * 得到样例在目标属性下的概率分布
 * 返回
 * {
 *	value1:[
 *		{"policyId":"001","Role#Sub":"doctor","Resource-id#Res":"script","Action-id#Act":"read"},
 *		{"policyId":"001","Role#Sub":"doctor","Resource-id#Res":"script","Action-id#Act":"write"}
 *	],
 *	value2:[
 *		{"policyId":"002","Role#Sub":"intern","Resource-id#Res":"script","Action-id#Act":"read"}
 *		{"policyId":"002","Role#Sub":"intern","Resource-id#Res":"script","Action-id#Act":"read"}
 *	],
 *	...
 *	}
 * isIgnore 是否忽略缺失属性的样例（是：添加缺失样例分支；否：将缺失样例添加到每一个分支中）
 */
ID3.prototype.getProbDistribution=function(examples,targetAttr,isIgnore){
	let prob={};
	for(let i=0;i<examples.length;i++){
		let value=examples[i][targetAttr];
		if(value!=null){
			prob[value]==null?prob[value]=[examples[i]]:	prob[value].push(examples[i]);
		}else if(!isIgnore){
			//样例中没有该属性，则在每一个可能中都添加这个样例
			for(let item of this.attr[targetAttr].values()){
				prob[item]==null?prob[item]=[examples[i]]:	prob[item].push(examples[i]);
			}
		}else{
			//样例中没有该属性时添加null分支
			prob["null"]==null?prob["null"]=[examples[i]]:	prob["null"].push(examples[i]);
		}
	}
	return prob;
}

